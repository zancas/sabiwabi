#! /usr/bin/env python3

import codecs
from matplotlib import pyplot 

color_dict = {  '2007': 'xkcd:red', 
                '2008': 'xkcd:orange',
                '2009': 'xkcd:yellow',
                '2010': 'xkcd:light green',
                '2011': 'xkcd:green',
                '2012': 'xkcd:light blue',
                '2013': 'xkcd:blue',
                '2014': 'xkcd:purple',
                '2015': 'xkcd:light violet',
                '2016': 'xkcd:pinkish',
                '2017': 'xkcd:terracotta',
                '2018': 'xkcd:salmon pink'}

def snarfcolumn(rows, columnnumber, delim=','):
    return [x.split(delim)[columnnumber-1] for x in rows]

def make_ints(data_column):
    return [data_column[0]] + [int(x) for x in data_column[1:]]

def _loaddata(filename='data/yop_annual_grants.csv'):
    with codecs.open(filename, encoding='utf-8') as fh:
        rows = fh.readlines()
    return rows

def plot_funded_versus_requested():
    print('hello world!')
    lines = _loaddata()
    amtreq = make_ints(snarfcolumn(lines, 4))
    amtfnd = make_ints(snarfcolumn(lines, 5))
    year = snarfcolumn(lines, 8)

    for x,y,c in zip(amtfnd[1:], amtreq[1:], year[1:]):
        pyplot.plot(x, y,'.',  color = color_dict[c])        
    pyplot.axis([0, max(amtfnd[1:]), 0, max(amtreq[1:])])
    pyplot.xlabel(amtfnd[0])
    pyplot.ylabel(amtreq[0])
    pyplot.savefig('pyplot2')


def main():
    plot_funded_versus_requested() 

if __name__ == '__main__':
    main()
